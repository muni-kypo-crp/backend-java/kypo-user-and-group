package cz.muni.ics.kypo.userandgroup.enums;

/**
 * The list of cache names
 */
public abstract class AbstractCacheNames {

    public static final String USERS_CACHE_NAME = "users";
}
